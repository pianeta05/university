Rails.application.routes.draw do
  resources :departments
  root to: 'pages#home'
  get '/not_yet', to: 'pages#not_yet', as: :not_yet
  resources :professors, except: [:new]
end
