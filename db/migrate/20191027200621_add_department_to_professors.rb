class AddDepartmentToProfessors < ActiveRecord::Migration[6.0]
  def change
    add_reference :professors, :department, foreign_key: true
  end
end
