class Department < ApplicationRecord
  validates :name, presence: true
  validates :building, presence: true
  validates :floor, presence: true
  has_many :professors
end
