json.extract! department, :id, :name, :building, :floor, :created_at, :updated_at
json.url department_url(department, format: :json)
